const inSoBtn = document.querySelector('#inSoBtn')

const chanLe = document.querySelector('#chanLe')
const whileFor = document.querySelector('#whileFor')
const method = document.querySelector('#method')

const outPutChanLe = document.querySelector('.outPutChanLe')


function xacDinhChanLe(chanLe) {
    return chanLe === "1" ? "CHAN" : "LE"

     
}

function xacDinhWhileFor(whileFor) {
    return whileFor === "1" ? "WHILE" : "FOR"

}


function xacDinhMethod(method) {
    return method === "1" ? "BUOC_NHAY" : "CHIA_HET_CHO_2"

}

function inKetQua (chanLe, whileFor, method) {
    let result = ""
    switch (chanLe) {
        case "CHAN": {
            if (whileFor === "WHILE") {
                switch (method) {
                    case "BUOC_NHAY": {
                        let i = 2; 
                        while (i < 100) {
                            result += i +"; ";
                            i += 2
                        }
                        break;
                    }
                        
                    default: {
                        let i = 1; 
                        while (i < 100) {
                            if (i%2 === 0) {
                                result += i +"; ";
                                ++i
                            }
                            ++i
                        }
                        break;
                    }
                }
            } else {
                switch (method) {
                    case "BUOC_NHAY": {
                        for (let i = 2; i < 100; i += 2) {
                            result += i +"; ";
                        }
                        break;
                    }
                        
                    default: {
                        for (let i = 1; i < 100; i++) {
                            if ( i%2 ===0) result += i +"; ";
                        }
                        break;
                    }
                }
            }
            break;
        }
            
    
        default: {
            if (whileFor === "WHILE") {
                switch (method) {
                    case "BUOC_NHAY": {
                        let i = 1; 
                        while (i < 100) {
                            result += i +"; ";
                            i += 2
                        }
                        break;
                    }
                        
                    default: {
                        let i = 1; 
                        while (i < 100) {
                            if (i%2 !== 0) {
                                result += i +"; ";
                                ++i
                            }
                            ++i
                        }
                        break;
                    }
                }
            } else {
                switch (method) {
                    case "BUOC_NHAY": {
                        for (let i = 1; i < 100; i += 2) {
                            result += i +"; ";
                        }
                        break;
                    }
                        
                    default: {
                        for (let i = 1; i < 100; i++) {
                            if ( i%2 !==0) result += i +"; ";
                        }
                        break;
                    }
                }
            }
            break;
        }
    }

    return result
}



inSoBtn.addEventListener('click', e => {
    let outPut
    e.preventDefault()
    outPut = inKetQua(xacDinhChanLe(chanLe.value), xacDinhWhileFor(whileFor.value), xacDinhMethod(method.value))

    outPutChanLe.innerHTML = `<tt>Kết quả chạy bằng ${xacDinhWhileFor(whileFor.value)} và dùng 
    ${xacDinhMethod(method.value)==="BUOC_NHAY" ? "BƯỚC NHHẢY" : "CHIA HẾT CHO 2"} là:<br> ${outPut}</tt>`

})