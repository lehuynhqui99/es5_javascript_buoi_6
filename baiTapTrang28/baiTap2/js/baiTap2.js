const inTongSoChanBtn = document.querySelector('#inTongSoChanBtn')

const number = document.querySelector('#number')
const whileFor2 = document.querySelector('#whileFor2')

const outPutTongSoChan = document.querySelector('.outPutTongSoChan')



function xacdinhWhileFor2 (whileFor) {
    switch (whileFor) {
        case "1": {
            return "WHILE";
        }
            
    
        default: {
            return "FOR";
        }
    }
}


function tinhTongSoChan(whileFor, number) {
    let tongSoChan = 0

    switch (whileFor) {
        case "WHILE": {
            let i = 1;
            while (i <= number) {
                if (i%2 ===0) {
                    tongSoChan += i
                }
                ++i
            }
            break;
        }
            
        default: {
            for (let i = 1; i <= number; ++i) {
                if (i%2 ===0) {
                    tongSoChan += i
                }
            }
            break;
        }
    }
    return tongSoChan;
}




inTongSoChanBtn.addEventListener('click', e => {
    e.preventDefault()
    
    // Validation

    if (!number.value || number.value.includes('.') || number.value.includes(',') || Number(number.value) <= 0) {
        alert("Tất cả các trường không được bỏ trống. Gia trị nhập vào phải là số nguyên dương.")
        return;
    } 

    let outPut = tinhTongSoChan(xacdinhWhileFor2 (whileFor2.value), Number(number.value))


    outPutTongSoChan.innerHTML =`Tổng các số chẵn từ 1 đến ${number.value}<br>Theo vòng lập ${xacdinhWhileFor2 (whileFor2.value)} là: <b>${outPut}</b>`
})