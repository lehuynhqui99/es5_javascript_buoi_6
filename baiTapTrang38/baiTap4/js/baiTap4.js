const inTheDivBtn = document.querySelector("#inTheDivBtn");

const outPutTheDiv = document.querySelector(".outPutTheDiv");

function inTheDiv() {
    let div =''
   for (let i = 1; i <= 10; ++i) {
       if ( i%2 === 0) {
           div += `<div class="bg-danger w-100 text-white" style="height: 30px">${i}</div>`

       } else {
           div += `<div class="bg-primary w-100 text-white" style="height: 30px">${i}</div>`
       }
   }
   return div;
}

inTheDivBtn.addEventListener('click', e => {
    e.preventDefault()

    let outPut = inTheDiv();

    outPutTheDiv.innerHTML = outPut

})