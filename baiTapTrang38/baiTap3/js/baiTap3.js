const inGiaiThuaBtn = document.querySelector("#inGiaiThuaBtn");
const soN = document.querySelector("#soN");

const outPutGiaiThua = document.querySelector(".outPutGiaiThua");

function tinhGiaiThua(n) {
   let giaiThua = 1; 

   for (let i = 1; i <= n; ++i) {
    giaiThua *= i
    }
   return giaiThua;
}

inGiaiThuaBtn.addEventListener('click', e => {
    e.preventDefault()

    // validation

    if (!soN.value || Number(soN.value) <= 0 || soN.value.includes(',') || soN.value.includes('.')) {
        alert("Tất cả các trường là bắt buộc, 'n' phải là số nguyên dương.")
    } else {
        let outPut;
        outPut = tinhGiaiThua(Number(soN.value));
    
        outPutGiaiThua.innerHTML = `Kết quả của phép tính là: <b>${outPut}</b>`
    }
})