const inSoNguyenDuongNhoNhatBtn = document.querySelector("#inSoNguyenDuongNhoNhatBtn");
const outPutSoNguyenDuongNhoNhat = document.querySelector(".outPutSoNguyenDuongNhoNhat");



function timSoNguyenDuongNhoNhat() {
    let tong = 0;
    let n
    for (let i = 1; i <= 9999; i++) {
        n = i;
        tong += i;

        if(tong > 10000) {
            break;
        }
    }
    return n;
}
inSoNguyenDuongNhoNhatBtn.addEventListener('click', e => {
    e.preventDefault()

    let outPut;
    outPut = timSoNguyenDuongNhoNhat();
    outPutSoNguyenDuongNhoNhat.innerHTML = `Số nguyên nhỏ nhất thoả yêu cầu bài toán là: <b>${outPut}</b>`
})