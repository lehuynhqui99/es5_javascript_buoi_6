const inSoNguyenToBtn = document.querySelector("#inSoNguyenToBtn");
const numberN = document.querySelector("#numberN");

const outPutSoNguyenTo = document.querySelector(".outPutSoNguyenTo");

function inSoNguyenTo(number) {
    let soNguyenTo ='';


    for (let i = 2; i<= number; ++i) {
        // Số nguyên tố đơn vị
        if(i === 2 || i === 3 || i === 5 || i === 7) {
            soNguyenTo += i + "; "
        } 

        if ( i> 10 &&i%2 !==0 && i%3 !==0 &&
            i%4 !==0 && i%5 !==0 &&
            i%6 !==0 && i%7 !==0 &&
            i%8 !==0 && i%9 !==0 && i%10 !==0) {
            soNguyenTo += i + "; "
        }
    }

    return soNguyenTo;
}

inSoNguyenToBtn.addEventListener('click', e => {
    e.preventDefault()

    // validation
    if (!numberN.value || Number(numberN.value) <=1 || numberN.value.includes('.') || numberN.value.includes(',')) {
        alert('Tất cả các trường là bắt buộc. Số nguyên tố phải là số nguyên dương lơn hơn 1. 0 và 1 không đuọc gọi là số nguyên tố. ')
        return;
    } 

    let outPut = inSoNguyenTo(Number(numberN.value));

    outPutSoNguyenTo.innerHTML = `Các số nguyên tố bé hơn hoặc bằng ${numberN.value} là: <br>${outPut}`

})