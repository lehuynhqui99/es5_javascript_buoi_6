const tinhTongBtn = document.querySelector("#tinhTongBtn");
const x = document.querySelector("#x");
const n = document.querySelector("#n");




const outPutTinhTong = document.querySelector(".outPutTinhTong");



function tinhTong(x, n) {
   let tong = 0; 

   for (let i =1; i <= n; ++i) {
    tong += Math.pow(x, i)
    }
   return tong;
}

tinhTongBtn.addEventListener('click', e => {
    e.preventDefault()

    // validation

    if (!x.value || !n.value || Number(x.value) < 0 || Number(n.value) <=0 || n.value.includes(',') || n.value.includes('.')) {
        alert("Tất cả các trường là bắt buộc, 'x' phải là số lớn hơn hoặc bằng 0; 'n' phải là số nguyên dương.")
    } else {
        let outPut;
        outPut = tinhTong(Number(x.value), Number(n.value));
    
        outPutTinhTong.innerHTML = `Kết quả của phép tính là: <b>${outPut}</b>`
    }
})